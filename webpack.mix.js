const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/bill_lines.js', 'public/js')
    .js('resources/js/bills.js', 'public/js')
    .js('resources/js/harvests.js', 'public/js')
    .js('resources/js/years.js', 'public/js')
    .js('resources/js/costs.js', 'public/js')
    .js('resources/js/wineyards.js', 'public/js')
    .js('resources/js/grapes.js', 'public/js')
    .js('resources/js/cost_types.js', 'public/js')
    .js('resources/js/payments.js', 'public/js')
    .js("resources/js/adminlte/adminlte.js", "public/js/adminlte")
    .js("resources/js/datepicker/bootstrap-datepicker.min.js", "public/js/datepicker")
    .sass("resources/css/app.scss", "public/css")
    .postCss('resources/css/datepicker/bootstrap-datepicker.css', 'public/css/datepicker')
    .copy('node_modules/admin-lte/dist/img', 'public/dist/img')
    //.postCss('resources/css/app.css', 'public/css', [
    //])
    .postCss('resources/css/app.css', 'public/css', [
    require('postcss-import'),
    require('tailwindcss'),
    require('autoprefixer'),
]);
