<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('home');
})->middleware(['auth'])->name('home');

// gestión Bill Lines
Route::get('/bill_lines', '\App\Http\Controllers\BillLineController@index')->name('bill_lines');
Route::get('/bill_lines/list', '\App\Http\Controllers\BillLineController@list')->name('bill_lines_list');
Route::get('/bill_lines/new', '\App\Http\Controllers\BillLineController@new')->name('bill_line_new');
Route::get('/bill_lines/save', '\App\Http\Controllers\BillLineController@save')->name('bill_line_save');
Route::get('/bill_lines/edit', '\App\Http\Controllers\BillLineController@edit')->name('bill_lines_edit');
Route::get('/bill_lines/edit_save', '\App\Http\Controllers\BillLineController@edit_save')->name('bill_lines_edit_save');
Route::get('/bill_lines/delete', '\App\Http\Controllers\BillLineController@delete')->name('bill_lines_delete');

Route::get('/bills', '\App\Http\Controllers\BillController@index')->name('bills');
Route::get('/bills/list', '\App\Http\Controllers\BillController@list')->name('bills_list');
Route::get('/bills/new', '\App\Http\Controllers\BillController@new')->name('bill_new');
Route::get('/bills/save', '\App\Http\Controllers\BillController@save')->name('bill_save');
Route::get('/bills/edit', '\App\Http\Controllers\BillController@edit')->name('bills_edit');
Route::get('/bills/edit_save', '\App\Http\Controllers\BillController@edit_save')->name('bills_edit_save');
Route::get('/bills/delete', '\App\Http\Controllers\BillController@delete')->name('bills_delete');
Route::get('/bills/info', '\App\Http\Controllers\BillController@info')->name('bills_info');

Route::get('/harvests', '\App\Http\Controllers\HarvestController@index')->name('harvests');
Route::get('/harvests/list', '\App\Http\Controllers\HarvestController@list')->name('harvest_list');

Route::get('/costs', '\App\Http\Controllers\CostController@index')->name('costs');
Route::get('/costs/list', '\App\Http\Controllers\CostController@list')->name('cost_list');
Route::get('/costs/new', '\App\Http\Controllers\CostController@new')->name('cost_new');
Route::get('/costs/save', '\App\Http\Controllers\CostController@save')->name('cost_save');
Route::get('/costs/edit', '\App\Http\Controllers\CostController@edit')->name('costs_edit');
Route::get('/costs/edit_save', '\App\Http\Controllers\CostController@edit_save')->name('costs_edit_save');
Route::get('/costs/delete', '\App\Http\Controllers\CostController@delete')->name('costs_delete');

Route::get('/payments', '\App\Http\Controllers\PaymentController@index')->name('payments');
Route::get('/payments/list', '\App\Http\Controllers\PaymentController@list')->name('payment_list');
Route::get('/payments/new', '\App\Http\Controllers\PaymentController@new')->name('payment_new');
Route::get('/payments/save', '\App\Http\Controllers\PaymentController@save')->name('payment_save');
Route::get('/payments/edit', '\App\Http\Controllers\PaymentController@edit')->name('payments_edit');
Route::get('/payments/edit_save', '\App\Http\Controllers\PaymentController@edit_save')->name('payments_edit_save');
Route::get('/payments/delete', '\App\Http\Controllers\PaymentController@delete')->name('payments_delete');

Route::get('/years', '\App\Http\Controllers\YearController@index')->name('years');
Route::get('/years/list', '\App\Http\Controllers\YearController@list')->name('year_list');

Route::get('/wineyards', '\App\Http\Controllers\WineyardController@index')->name('wineyards');
Route::get('/wineyards/list', '\App\Http\Controllers\WineyardController@list')->name('wineyard_list');
Route::get('/wineyards/new', '\App\Http\Controllers\WineyardController@new')->name('wineyard_new');
Route::get('/wineyards/save', '\App\Http\Controllers\WineyardController@save')->name('wineyard_save');
Route::get('/wineyards/edit', '\App\Http\Controllers\WineyardController@edit')->name('wineyards_edit');
Route::get('/wineyards/edit_save', '\App\Http\Controllers\WineyardController@edit_save')->name('wineyards_edit_save');
Route::get('/wineyards/delete', '\App\Http\Controllers\WineyardController@delete')->name('wineyards_delete');

Route::get('/grapes', '\App\Http\Controllers\GrapeController@index')->name('grapes');
Route::get('/grapes/list', '\App\Http\Controllers\GrapeController@list')->name('grape_list');
Route::get('/grapes/new', '\App\Http\Controllers\GrapeController@new')->name('grape_new');
Route::get('/grapes/save', '\App\Http\Controllers\GrapeController@save')->name('grape_save');
Route::get('/grapes/edit', '\App\Http\Controllers\GrapeController@edit')->name('grapes_edit');
Route::get('/grapes/edit_save', '\App\Http\Controllers\GrapeController@edit_save')->name('grapes_edit_save');
Route::get('/grapes/delete', '\App\Http\Controllers\GrapeController@delete')->name('grapes_delete');

Route::get('/cost_types', '\App\Http\Controllers\CostTypeController@index')->name('cost_types');
Route::get('/cost_types/list', '\App\Http\Controllers\CostTypeController@list')->name('cost_type_list');
Route::get('/cost_types/new', '\App\Http\Controllers\CostTypeController@new')->name('cost_type_new');
Route::get('/cost_types/save', '\App\Http\Controllers\CostTypeController@save')->name('cost_type_save');
Route::get('/cost_types/edit', '\App\Http\Controllers\CostTypeController@edit')->name('cost_types_edit');
Route::get('/cost_types/edit_save', '\App\Http\Controllers\CostTypeController@edit_save')->name('cost_types_edit_save');
Route::get('/cost_types/delete', '\App\Http\Controllers\CostTypeController@delete')->name('cost_types_delete');

require __DIR__.'/auth.php';
