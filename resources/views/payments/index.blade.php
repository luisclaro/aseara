@extends('layouts.app')

@section('content')

@include('partials.modal-default', ['id'=>'modal-nuevo','titulo'=>'Nuevo Pago','tamanho'=>'modal-lg'])
@include('partials.modal-default', ['id'=>'modal-editar','titulo'=>'Editar Pago','tamanho'=>'modal-lg'])
@include('partials.modal-default', ['id'=>'modal-eliminar','titulo'=>'Eliminar Pago','tamanho'=>'modal-sm'])

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">PAGOS</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">
                <table class="table table-hover text-nowrap" id="tabla_listado_payments">
                    <thead>
                        <tr>
                            <th>FECHA</th>
                            <th>FACTURA</th>
                            <th>IMPORTE</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- ajax -->
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>

<!-- nuevo usuario -->
<button type="button" class="nuevo btn bg-gradient-success">Añadir Pago</button>



@endsection

@section('scripts')
    <script src="{{ asset('js/payments.js') }}"></script>
@stop
