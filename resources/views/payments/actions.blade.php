<a class="editar btn bg-gradient-primary btn-sm" href="#" data-id="{{ $payments->id }}">
    <i class="fas fa-pencil-alt"></i>
    Editar
</a>
<a class="eliminar btn bg-gradient-danger btn-sm" href="#" data-id="{{ $payments->id }}" data-amount="{{ $payments->amount }}" data-date="{{ $payments->date }}">
    <i class="fas fa-trash"></i>
    Eliminar
</a>