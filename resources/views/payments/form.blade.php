<form class="form-horizontal" id="formulario" action="#">
    <input type="hidden" class="form-control" id="id" name="id" value="{{ $payment->id ?? '' }}">

    <div class="form-group row">
        <label for="date" class="col-sm-3 col-form-label">Fecha</label>
        <div class="col-sm-9">

            <input class="form-control datepicker" id="date" name="date" placeholder="MM/DD/YYYY" type="text"/ autocomplete="on" value="{{ $payment->date ?? '' }}">

        </div>
    </div>

    <div class="form-group row">
        <label for="bill_id" class="col-sm-3 col-form-label">Factura</label>
        <div class="col-sm-9">
            <select class="form-control" id="bill_id" name="bill_id">
                @foreach ($bills as $bill)
                    <option value="{{ $bill->id }}"
                        @isset ($payment)
                            @if ($bill->id==$payment->bill_id)  selected
                        @endif
                        @elseif ($loop->first)  selected @endisset
                    >
                        {{ $bill->identifier }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group row">
        <label for="amount" class="col-sm-3 col-form-label">Importe (€)</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="amount" name="amount" placeholder="" autocomplete="on" value="{{ $payment->amount ?? '' }}">
        </div>
    </div>


</form>

