@extends('layouts.app')

@section('content')

@include('partials.modal-default', ['id'=>'modal-nuevo','titulo'=>'Nueva Tipo de Gasto','tamanho'=>'modal-lg'])
@include('partials.modal-default', ['id'=>'modal-editar','titulo'=>'Editar Tipo de Gasto','tamanho'=>'modal-lg'])
@include('partials.modal-default', ['id'=>'modal-eliminar','titulo'=>'Eliminar Tipo de Gasto','tamanho'=>'modal-sm'])

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">TIPOS DE GASTO</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">


                <table class="table table-hover text-nowrap" id="tabla_listado_cost_types">
                    <thead>
                        <tr>
                            <th>NOMBRE</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- ajax -->
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>

<!-- nuevo usuario -->
<button type="button" class="nuevo btn bg-gradient-success">Añadir nueva</button>



@endsection

@section('scripts')
    <script src="{{ asset('js/cost_types.js') }}"></script>
@stop
