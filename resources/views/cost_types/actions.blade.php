<a class="editar btn bg-gradient-primary btn-sm" href="#" data-id="{{ $cost_types->id }}">
    <i class="fas fa-pencil-alt"></i>
    Editar
</a>
<a class="eliminar btn bg-gradient-danger btn-sm" href="#" data-id="{{ $cost_types->id }}" data-subject="{{ $cost_types->subject }}">
    <i class="fas fa-trash"></i>
    Eliminar
</a>