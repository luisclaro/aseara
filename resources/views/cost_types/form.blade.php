<form class="form-horizontal" id="formulario" action="#">
    <input type="hidden" class="form-control" id="id" name="id" value="{{ $cost_type->id ?? '' }}">

    <div class="form-group row">
        <label for="subject" class="col-sm-3 col-form-label">Nombre</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="subject" name="subject" placeholder="" autocomplete="on" value="{{ $cost_type->subject ?? '' }}">
        </div>
    </div>

</form>