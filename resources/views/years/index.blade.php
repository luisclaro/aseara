@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">AÑOS FISCALES</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">


                <table class="table table-hover text-nowrap" id="tabla_listado_years">
                    <thead>
                        <tr>
                            <th>FECHA</th>
                            <th>PAGOS</th>
                            <th>GASTOS</th>
                            <th>RESULTADO</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- ajax -->
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('js/years.js') }}"></script>
@stop
