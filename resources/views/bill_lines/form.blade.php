<form class="form-horizontal" id="formulario" action="#">
    <input type="hidden" class="form-control" id="id" name="id" value="{{ $bill_line->id ?? '' }}">

    <div class="form-group row">
        <label for="imsi" class="col-sm-3 col-form-label">Precio (€)</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="price" name="price" placeholder="" autocomplete="on" value="{{ $bill_line->price ?? '' }}">
        </div>
    </div>

    <div class="form-group row">
        <label for="alcohol" class="col-sm-3 col-form-label">Grado</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="alcohol" name="alcohol" placeholder="" autocomplete="on" value="{{ $bill_line->alcohol ?? '' }}">
        </div>
    </div>

    <div class="form-group row">
        <label for="quality" class="col-sm-3 col-form-label">Calidad</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="quality" name="quality" placeholder="" autocomplete="on" value="{{ $bill_line->quality ?? '' }}">
        </div>
    </div>

    <div class="form-group row">
        <label for="kg" class="col-sm-3 col-form-label">Kilos</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="kg" name="kg" placeholder="" autocomplete="on" value="{{ $bill_line->kg ?? '' }}">
        </div>
    </div>

    <div class="form-group row">
        <label for="bill_id" class="col-sm-3 col-form-label">Facturas</label>
        <div class="col-sm-9">
            <select class="form-control" id="bill_id" name="bill_id">
                @foreach ($bills as $bill)
                    <option value="{{ $bill->id }}"
                        @isset ($bill_line)
                            @if ($bill->id==$bill_line->bill_id)  selected
                        @endif
                        @elseif ($loop->first)  selected @endisset
                    >
                        {{ $bill->identifier }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group row">
        <label for="grape_id" class="col-sm-3 col-form-label">Variedad</label>
        <div class="col-sm-9">
            <select class="form-control" id="grape_id" name="grape_id">
                @foreach ($grapes as $grape)
                    <option value="{{ $grape->id }}"
                        @isset ($bill_line)
                            @if ($grape->id==$bill_line->grape_id)  selected
                        @endif
                        @elseif ($loop->first)  selected @endisset
                    >
                        {{ $grape->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group row">
        <label for="wineyard_id" class="col-sm-3 col-form-label">Parcela</label>
        <div class="col-sm-9">
            <select class="form-control" id="wineyard_id" name="wineyard_id">
                @foreach ($wineyards as $wineyard)
                    <option value="{{ $wineyard->id }}"
                        @isset ($bill_line)
                            @if ($wineyard->id==$bill_line->wineyard_id)  selected
                        @endif
                        @elseif ($loop->first)  selected @endisset
                    >
                        {{ $wineyard->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>

</form>

