@extends('layouts.app')

@section('content')

@include('partials.modal-default', ['id'=>'modal-nuevo','titulo'=>'Nueva Línea de Factura','tamanho'=>'modal-lg'])
@include('partials.modal-default', ['id'=>'modal-editar','titulo'=>'Editar Línea de Factura','tamanho'=>'modal-lg'])
@include('partials.modal-default', ['id'=>'modal-eliminar','titulo'=>'Eliminar Línea de Factura','tamanho'=>'modal-sm'])

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Líneas de factura</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">


                <table class="table table-hover text-nowrap" id="tabla_listado_bill_lines">
                    <thead>
                        <tr>
                            <th>FECHA</th>
                            <th>FACTURA</th>
                            <th>KILOS</th>
                            <th>VARIEDAD</th>
                            <th>PARCELA</th>
                            <th>CALIDAD</th>
                            <th>GRADO</th>
                            <th>PRECIO<br>(CON IVA)</th>
                            <th>PRECIO<br>(SIN IVA)</th>
                            <th>IMPORTE<br>(SIN IVA)</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- ajax -->
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>

<!-- nuevo usuario -->
<button type="button" class="nuevo btn bg-gradient-success">Añadir nueva</button>



@endsection

@section('scripts')
    <script src="{{ asset('js/bill_lines.js') }}"></script>
@stop
