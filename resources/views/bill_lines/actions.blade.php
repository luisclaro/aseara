<a class="editar btn bg-gradient-primary btn-sm" href="#" data-id="{{ $bill_lines->id }}">
    <i class="fas fa-pencil-alt"></i>
    Editar
</a>
<a class="eliminar btn bg-gradient-danger btn-sm" href="#" data-id="{{ $bill_lines->id }}" data-identifier="{{ $bill_lines->bill->identifier }}" data-kg="{{ $bill_lines->kg }}">
    <i class="fas fa-trash"></i>
    Eliminar
</a>