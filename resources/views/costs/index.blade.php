@extends('layouts.app')

@section('content')

@include('partials.modal-default', ['id'=>'modal-nuevo','titulo'=>'Nuevo Gasto','tamanho'=>'modal-lg'])
@include('partials.modal-default', ['id'=>'modal-editar','titulo'=>'Editar Gasto','tamanho'=>'modal-lg'])
@include('partials.modal-default', ['id'=>'modal-eliminar','titulo'=>'Eliminar Gasto','tamanho'=>'modal-sm'])

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">GASTOS</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">


                <table class="table table-hover text-nowrap" id="tabla_listado_costs">
                    <thead>
                        <tr>
                            <th>VENDIMIA</th>
                            <th>AÑO<br>FACTURA</th>
                            <th>GASTOS</th>
                            <th>PARCELA</th>
                            <th>CONCEPTO</th>
                            <th>COMENTARIO</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- ajax -->
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>

<!-- nuevo usuario -->
<button type="button" class="nuevo btn bg-gradient-success">Añadir Gasto</button>



@endsection

@section('scripts')
    <script src="{{ asset('js/costs.js') }}"></script>
@stop