<form class="form-horizontal" id="formulario" action="#">
    <input type="hidden" class="form-control" id="id" name="id" value="{{ $cost->id ?? '' }}">
    <div class="form-group row">
        <label for="imsi" class="col-sm-3 col-form-label">Año Vendimia</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="harvest" name="harvest" placeholder="{{ now()->year }}" autocomplete="on" value="{{ $cost->harvest ?? now()->year }}" maxlength="4" minlength="4">
        </div>
    </div>

    <div class="form-group row">
        <label for="year" class="col-sm-3 col-form-label">Año Facturación</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="year" name="year" placeholder="{{ now()->year }}" autocomplete="on" value="{{ $cost->year ?? now()->year }}"  maxlength="4" minlength="4">
        </div>
    </div>

    <div class="form-group row">
        <label for="gastos" class="col-sm-3 col-form-label">Gasto (€)</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="gastos" name="gastos" placeholder="" autocomplete="on" value="{{ $cost->gastos ?? '' }}">
        </div>
    </div>

    <div class="form-group row">
        <label for="comment" class="col-sm-3 col-form-label">Comentario</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="comment" name="comment" placeholder="" autocomplete="on" value="{{ $cost->comment ?? '' }}">
        </div>
    </div>
    <div class="form-group row">
        <label for="wineyard_id" class="col-sm-3 col-form-label">Parcela</label>
        <div class="col-sm-9">
            <select class="form-control" id="wineyard_id" name="wineyard_id">
                @foreach ($wineyards as $wineyard)
                    <option value="{{ $wineyard->id }}"
                        @isset ($cost)
                            @if ($wineyard->id==$cost->wineyard_id)  selected
                        @endif
                        @elseif ($loop->first)  selected @endisset
                    >
                        {{ $wineyard->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group row">
        <label for="cost_type_id" class="col-sm-3 col-form-label">Tipo de Gasto</label>
        <div class="col-sm-9">
            <select class="form-control" id="cost_type_id" name="cost_type_id">
                @foreach ($cost_types as $cost_type)
                    <option value="{{ $cost_type->id }}"
                        @isset ($cost)
                            @if ($cost_type->id==$cost->cost_type_id)  selected
                        @endif
                        @elseif ($loop->first)  selected @endisset
                    >
                        {{ $cost_type->subject }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>

</form>

