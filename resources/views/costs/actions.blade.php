<a class="editar btn bg-gradient-primary btn-sm" href="#" data-id="{{ $costs->id }}">
    <i class="fas fa-pencil-alt"></i>
    Editar
</a>
<a class="eliminar btn bg-gradient-danger btn-sm" href="#" data-id="{{ $costs->id }}" data-gastos="{{ $costs->gastos }}" data-harvest="{{ $costs->harvest }}">
    <i class="fas fa-trash"></i>
    Eliminar
</a>