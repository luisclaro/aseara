<div class="modal fade" id="{{ $id }}">
    <div class="modal-dialog {{ $tamanho }}">
        <div class="modal-content">
           <div id="{{ $id }}-loading" class="overlay d-flex justify-content-center align-items-center" style="display: none !important">
                <i class="fas fa-2x fa-sync fa-spin"></i>
            </div>
            <div class="modal-header">
                <h4 class="modal-title" id="modal-title-{{ $id }}">{{ $titulo }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-body-{{ $id }}">
                <p>Body</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" id="modal-btn-cerrar-{{ $id }}" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" id="modal-btn-guardar-{{ $id }}" class="btn btn-primary">Guardar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->