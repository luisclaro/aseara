    <aside class="main-sidebar sidebar-dark-primary elevation-4">

        <div class="sidebar">

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          <li class="nav-item ">
              <a href="{{ route('harvests') }}" class="nav-link active">
                  <i class="nav-icon fas fa-tachometer-alt"></i>
                  <p>VENDIMIAS</p>
              </a>
          </li>
          <li class="nav-item ">
              <a href="{{ route('years') }}" class="nav-link active">
                  <i class="nav-icon fas fa-tachometer-alt"></i>
                  <p>AÑOS FISCALES</p>
              </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('bill_lines') }}" class="nav-link">
              <p>Lineas de factura</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('bills') }}" class="nav-link">
              <p>Facturas</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('costs') }}" class="nav-link">
              <p>Gastos</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('payments') }}" class="nav-link">
              <p>Pagos</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('wineyards') }}" class="nav-link">
              <p>Parcelas</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('grapes') }}" class="nav-link">
              <p>Variedad de uvas</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('cost_types') }}" class="nav-link">
              <p>Tipo de gasto</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
        </div>
    </aside>
