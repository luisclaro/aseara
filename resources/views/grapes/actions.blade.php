<a class="editar btn bg-gradient-primary btn-sm" href="#" data-id="{{ $grapes->id }}">
    <i class="fas fa-pencil-alt"></i>
    Editar
</a>
<a class="eliminar btn bg-gradient-danger btn-sm" href="#" data-id="{{ $grapes->id }}" data-name="{{ $grapes->name }}">
    <i class="fas fa-trash"></i>
    Eliminar
</a>