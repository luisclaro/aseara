<a class="info btn bg-gradient-primary btn-sm" href="#" data-id="{{ $bills->id }}">
    <i class="fas fa-wifi"></i>
    Líneas
</a>
<a class="editar btn bg-gradient-primary btn-sm" href="#" data-id="{{ $bills->id }}">
    <i class="fas fa-pencil-alt"></i>
    Editar
</a>
<a class="eliminar btn bg-gradient-danger btn-sm" href="#" data-id="{{ $bills->id }}" data-identifier="{{ $bills->identifier }}" data-date="{{ $bills->date }}">
    <i class="fas fa-trash"></i>
    Eliminar
</a>