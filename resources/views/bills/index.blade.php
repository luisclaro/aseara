@extends('layouts.app')

@section('content')

@include('partials.modal-default', ['id'=>'modal-nuevo','titulo'=>'Nueva Factura','tamanho'=>'modal-lg'])
@include('partials.modal-default', ['id'=>'modal-editar','titulo'=>'Editar Factura','tamanho'=>'modal-lg'])
@include('partials.modal-default', ['id'=>'modal-eliminar','titulo'=>'Eliminar Factura','tamanho'=>'modal-sm'])
@include('partials.modal-default', ['id'=>'modal-info','titulo'=>'Líneas de Factura','tamanho'=>'modal-lg'])

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Facturas</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">


                <table class="table table-hover text-nowrap" id="tabla_listado_bills">
                    <thead>
                        <tr>
                            <th>Cosecha</th>
                            <th>Identificador</th>
                            <th>Fecha</th>
                            <th>Beneficiario</th>
                            <th>Total Bruto</th>
                            <th>Iva</th>
                            <th>IRPF</th>
                            <th>Total FACTURA</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- ajax -->
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>

<!-- nuevo usuario -->
<button type="button" class="nuevo btn bg-gradient-success">Añadir nueva</button>



@endsection

@section('scripts')
    <script src="{{ asset('js/bills.js') }}"></script>
@stop
