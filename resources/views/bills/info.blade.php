<table class="table table-hover text-nowrap" id="tabla_listado_bill_lines">
    <thead>
        <tr>
            <th>KILOS</th>
            <th>VARIEDAD</th>
            <th>PARCELA</th>
            <th>CALIDAD</th>
            <th>GRADO</th>
            <th>PRECIO<br>(CON IVA)</th>
            <th>PRECIO<br>(SIN IVA)</th>
            <th>IMPORTE<br>(SIN IVA)</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($bill_lines as $bill_line)
            <tr>
                <td> {{ $bill_line->kg }} </td>
                <td> {{ $bill_line->grape->name }} </td>
                <td> {{ $bill_line->wineyard->name }} </td>
                <td> {{ $bill_line->quality }} </td>
                <td> {{ $bill_line->alcohol }} </td>
                <td> {{ $bill_line->precio_con_iva }} </td>
                <td> {{ $bill_line->price }} </td>
                <td> {{ $bill_line->importe }} </td>
            </tr>
        @endforeach
    </tbody>
</table>