<form class="form-horizontal" id="formulario" action="#">
    <input type="hidden" class="form-control" id="id" name="id" value="{{ $bill->id ?? '' }}">

    <div class="form-group row">
        <label for="date" class="col-sm-3 col-form-label">Fecha</label>
        <div class="col-sm-9">

            <input class="form-control  datepicker" id="date" name="date" placeholder="DD-MM-YYYY" type="text"/ autocomplete="on" value="{{ $bill->date ?? '' }}">

        </div>
    </div>

    <div class="form-group row">
        <label for="imsi" class="col-sm-3 col-form-label">Año Vendimia</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="harvest" name="harvest" placeholder="{{ now()->year }}" autocomplete="on" value="{{ now()->year }}" maxlength="4" minlength="4">
        </div>
    </div>

    <div class="form-group row">
        <label for="identifier" class="col-sm-3 col-form-label">Identificador</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="identifier" name="identifier" placeholder="" autocomplete="on" value="{{ $bill->identifier ?? '' }}">
        </div>
    </div>

    <div class="form-group row">
        <label for="receptor" class="col-sm-3 col-form-label">Receptor</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="receptor" name="receptor" placeholder="" autocomplete="on" value="{{ $bill->receptor ?? '' }}">
        </div>
    </div>
    <div class="form-group row">
        <label for="iva" class="col-sm-3 col-form-label">Iva</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="iva" name="iva" placeholder="" autocomplete="on" value="12">
        </div>
    </div>

    <div class="form-group row">
        <label for="irpf" class="col-sm-3 col-form-label">Irpf</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="irpf" name="irpf" placeholder="" autocomplete="on" value="2">
        </div>
    </div>



</form>

