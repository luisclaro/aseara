<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="csrf-param" content="_token" />

    <title>{{ config('app.name', 'A Seara') }}</title>

    <!-- Scripts -->


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datepicker/bootstrap-datepicker.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    @toastr_css
    <!-- Styles -->


</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        @include('partials.nav')
        @include('partials.lateral')
        <div class="content-wrapper">
            <section class="content p-0">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h3>  @yield('title')</h3>
                        </div>
                        <div class="col-sm-6">

                        </div>
                    </div>

                    @yield('content')
                    @include('modals.delete')
                </div>
            </section>
        </div>

</div>
<script src="{{ asset('js/app.js') }}"></script>

<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>

<script src="{{ asset('js/adminlte/adminlte.js') }}"></script>


<link href="http://127.0.0.1:8000/css/datepicker/bootstrap-datepicker.css" rel="stylesheet">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<!--
<link href="{{ asset('css/datepicker/bootstrap-datepicker.css') }}" rel="stylesheet">
<script src="{{ asset('js/datepicker/bootstrap-datepicker.min.js') }}"></script>
-->




<script>
    window.datatableColumns = {!! json_encode(__('javascript'))!!};
</script>
    @toastr_js
    @toastr_render
    @yield('scripts')
</body>
</html>
