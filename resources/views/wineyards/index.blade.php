@extends('layouts.app')

@section('content')

@include('partials.modal-default', ['id'=>'modal-nuevo','titulo'=>'Nueva Parcela','tamanho'=>'modal-lg'])
@include('partials.modal-default', ['id'=>'modal-editar','titulo'=>'Editar Parcela','tamanho'=>'modal-lg'])
@include('partials.modal-default', ['id'=>'modal-eliminar','titulo'=>'Eliminar Parcela','tamanho'=>'modal-sm'])

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">PARCELAS</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">


                <table class="table table-hover text-nowrap" id="tabla_listado_wineyards">
                    <thead>
                        <tr>
                            <th>NOMBRE</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- ajax -->
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>

<!-- nuevo usuario -->
<button type="button" class="nuevo btn bg-gradient-success">Añadir nueva</button>



@endsection

@section('scripts')
    <script src="{{ asset('js/wineyards.js') }}"></script>
@stop
