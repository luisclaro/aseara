<form class="form-horizontal" id="formulario" action="#">
    <input type="hidden" class="form-control" id="id" name="id" value="{{ $wineyard->id ?? '' }}">

    <div class="form-group row">
        <label for="name" class="col-sm-3 col-form-label">Nombre</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="name" name="name" placeholder="" autocomplete="on" value="{{ $wineyard->name ?? '' }}">
        </div>
    </div>

</form>