@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">VENDIMIAS</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">


                <table class="table table-hover text-nowrap" id="tabla_listado_harvests">
                    <thead>
                        <tr>
                            <th>AÑO</th>
                            <th>INGRESOS</th>
                            <th>GASTOS</th>
                            <th>RESULTADO</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- ajax -->
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('js/harvests.js') }}"></script>
@stop