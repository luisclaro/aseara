$(function () {
    cargar_listado_years();
});

function cargar_listado_years () {

    $("#tabla_listado_years").DataTable({
        "ajax": {
            "url": "years/list",
            "type": "GET"
        },
        columns: [
            {data: 'harvest', name: 'harvest'},
            {data: 'ingresos', name: 'ingresos', render: DataTable.render.number( '.', ',', 2, '', ' €' )},
            {data: 'gastos', name: 'gastos', render: DataTable.render.number( '.', ',', 2, '', ' €' )},
            {data: 'resultado', name: 'resultado', render: DataTable.render.number( '.', ',', 2, '', ' €' )},
        ],
        "order": [[ 0, "desc" ]],
    });
}
