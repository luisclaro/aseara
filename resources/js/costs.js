$(function () {
    $('#tabla_listado_costs').on("click", '.editar', function(event) {
        editar($(this).attr('data-id'));
    });

    $('#tabla_listado_costs').on("click", '.eliminar', function(event) {
        eliminar($(this).attr('data-id'), $(this).attr('data-gastos'), $(this).attr('data-harvest'));
    });

    $('.nuevo').on('click', function() {
        nuevo();
    });

    cargar_listado_costs();
});

function cargar_listado_costs () {

    $("#tabla_listado_costs").DataTable({
        "ajax": {
            "url": "costs/list",
            "type": "GET"
        },
        columns: [
            {data: 'harvest', name: 'harvest'},
            {data: 'year', name: 'year'},
            {data: 'gastos', name: 'gastos', render: DataTable.render.number( '.', ',', 2, '', ' €' )},
            {data: 'wineyard', name: 'wineyard'},
            {data: 'cost_subject', name: 'cost_subject'},
            {data: 'comment', name: 'comment'},
            {data: 'acciones', name: 'Acciones', orderable: false, searchable: false}
        ],
        "order": [[ 0, "desc" ]],
    });
}

function nuevo () {
    $.ajax({
        type: "GET",
        url: 'costs/new',
        beforeSend: function() {
            $('#modal-editar .modal-body').html('');
            $('#modal-btn-guardar-modal-nuevo').off();
            $('#modal-nuevo .modal-body').html('<div class="text-center"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>');
            $('#modal-nuevo').modal('show');
            return true;
        },
        success: function(data) {
            $('#modal-nuevo .modal-body').html(data);
            $('#modal-btn-guardar-modal-nuevo').on('click',function() {
                nuevo_guardar();
            })
            return true;
        },
        error: function() {
            $('#modal-nuevo .modal-body').html('Error obteniendo datos');
            return true;
        }
    });
    return true;
}

function nuevo_guardar () {
    $.ajax({
        type: "GET",
        url: 'costs/save',
        data: $('#formulario').serialize(),
        beforeSend: function () {
            //$('#modal-btn-guardar-modal-nuevo').off();
            $("#modal-nuevo input").removeClass("is-invalid");
            $('#modal-btn-guardar-modal-nuevo').prop("disabled",true);
            $('#modal-nuevo-loading').removeAttr("style");
            return true;
        },
        success: function (data) {
            $('#modal-nuevo').modal('hide');
            $("#tabla_listado_costs").DataTable().ajax.reload();
            toastr.success('GASTO guardado correctamente');
            return true;
        },
        error: function (data) {
            if(data.status == 422) {
                $.each(data.responseJSON.errors, function (key, item) {
                    $("#"+key).addClass('is-invalid');
                    toastr.error(item);
                });
            } else {
                $('#modal-nuevo .modal-body').html('Error desconocido guardando datos: ['+data.status+']');
            }
            return true;
        },
        complete: function () {
            $('#modal-nuevo-loading').attr('style','display: none !important');
            $('#modal-btn-guardar-modal-nuevo').prop("disabled",false);
            return true;
        }
    });
    return true;
}

function editar(id) {
    $.ajax({
        type: "GET",
        url: 'costs/edit',
        data: {id:id},
        beforeSend: function() {
            $('#modal-nuevo .modal-body').html('');
            $('#modal-btn-guardar-modal-editar').off();
            $('#modal-editar .modal-body').html('<div class="text-center"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>');
            $('#modal-editar').modal('show');
            return true;
        },
        success: function(data) {
            $('#modal-editar .modal-body').html(data);
            $('#modal-btn-guardar-modal-editar').on('click',function() {
                editar_guardar(id);
            })
            return true;
        },
        error: function() {
            $('#modal-editar .modal-body').html('Error obteniendo datos');
            return true;
        }
    });
    return true;
}

function editar_guardar(id) {
        $.ajax({
        type: "GET",
        url: 'costs/edit_save',
        data: $('#formulario').serialize(),
        beforeSend: function() {
            //$('#modal-btn-guardar-modal-editar').off();
            $('#modal-btn-guardar-modal-editar').prop("disabled",true);
            $("#modal-editar input").removeClass("is-invalid");
            $('#modal-editar-loading').removeAttr("style");
            return true;
        },
        success: function(data) {
            $('#modal-editar').modal('hide');
            $("#tabla_listado_costs").DataTable().ajax.reload();
            toastr.success('Gasto actualizado correctamente');
            return true;
        },
        error: function(data) {
            if(data.status == 422) {
                $.each(data.responseJSON.errors, function (key, item) {
                    $("#"+key).addClass('is-invalid');
                    toastr.error(item);
                });
            } else {
                $('#modal-editar .modal-body').html('Error desconocido guardando datos: ['+data.status+']');
            }
            return true;
        },
        complete: function() {
            $('#modal-editar-loading').attr('style','display: none !important');
            $('#modal-btn-guardar-modal-editar').prop("disabled",false);
            return true;
        }
    });
    return true;
}

function eliminar (id, gastos, harvest) {
    $('#modal-btn-guardar-modal-eliminar').show();
    $('#modal-eliminar .modal-body').html('¿Seguro que quieres eliminar gasto: '+ gastos +'€. Cosecha: '+ harvest +' ? ');
    $('#modal-btn-guardar-modal-eliminar').html('ELIMINAR');
    $('#modal-btn-guardar-modal-eliminar').on('click',function() {
        _eliminar(id);
    })
    $('#modal-eliminar').modal('show');
    return false;
}

function _eliminar (id) {
    $.ajax({
        type: "GET",
        url: 'costs/delete',
        data: {id: id},
        beforeSend: function () {
            $('#modal-btn-guardar-modal-eliminar').off();
            $('#modal-btn-guardar-modal-eliminar').prop("disabled",true);
            $('#modal-eliminar .modal-body').html('<div class="text-center"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>');
            return true;
        },

        success: function (data) {
            if (data.errors == 'fail') {
                $('#modal-btn-guardar-modal-eliminar').hide();
                $('#modal-eliminar .modal-body').html(data.message);
            }
            else {
                $('#modal-eliminar').modal('hide');
                $("#tabla_listado_costs").DataTable().ajax.reload();
                toastr.success(data.message);
            }
            return true;
        },

        error: function () {
            $('#modal-eliminar .modal-body').html('Error eliminando datos');
            return true;
        },

        complete: function() {
            $('#modal-btn-guardar-modal-eliminar').prop("disabled",false);
            return true;
        }
    });
}