$(function () {
    $('#tabla_listado_cost_types').on("click", '.editar', function(event) {
        editar($(this).attr('data-id'));
    });

    $('#tabla_listado_cost_types').on("click", '.eliminar', function(event) {
        eliminar($(this).attr('data-id'), $(this).attr('data-subject'));
    });

    $('.nuevo').on('click', function() {
        nuevo();
    });

    cargar_listado_cost_types();
});

function cargar_listado_cost_types () {

    $("#tabla_listado_cost_types").DataTable({
        "ajax": {
            "url": "cost_types/list",
            "type": "GET"
        },
        columns: [
            {data: 'subject', name: 'subject'},
            {data: 'acciones', name: 'Acciones', orderable: false, searchable: false}
        ],
        "order": [[ 0, "desc" ]],
    });
}

function nuevo () {
    $.ajax({
        type: "GET",
        url: 'cost_types/new',
        beforeSend: function() {
            $('#modal-editar .modal-body').html('');
            $('#modal-btn-guardar-modal-nuevo').off();
            $('#modal-nuevo .modal-body').html('<div class="text-center"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>');
            $('#modal-nuevo').modal('show');
            return true;
        },
        success: function(data) {
            $('#modal-nuevo .modal-body').html(data);
            $('#modal-btn-guardar-modal-nuevo').on('click',function() {
                nuevo_guardar();
            })
            return true;
        },
        error: function() {
            $('#modal-nuevo .modal-body').html('Error obteniendo datos');
            return true;
        }
    });
    return true;
}

function nuevo_guardar () {
    $.ajax({
        type: "GET",
        url: 'cost_types/save',
        data: $('#formulario').serialize(),
        beforeSend: function () {
            //$('#modal-btn-guardar-modal-nuevo').off();
            $("#modal-nuevo input").removeClass("is-invalid");
            $('#modal-btn-guardar-modal-nuevo').prop("disabled",true);
            $('#modal-nuevo-loading').removeAttr("style");
            return true;
        },
        success: function (data) {
            $('#modal-nuevo').modal('hide');
            $("#tabla_listado_cost_types").DataTable().ajax.reload();
            toastr.success('Tipo de gasto guardado correctamente');
            return true;
        },
        error: function (data) {
            if(data.status == 422) {
                $.each(data.responseJSON.errors, function (key, item) {
                    $("#"+key).addClass('is-invalid');
                    toastr.error(item);
                });
            } else {
                $('#modal-nuevo .modal-body').html('Error desconocido guardando datos: ['+data.status+']');
            }
            return true;
        },
        complete: function () {
            $('#modal-nuevo-loading').attr('style','display: none !important');
            $('#modal-btn-guardar-modal-nuevo').prop("disabled",false);
            return true;
        }
    });
    return true;
}

function editar(id) {
    $.ajax({
        type: "GET",
        url: 'cost_types/edit',
        data: {id:id},
        beforeSend: function() {
            $('#modal-nuevo .modal-body').html('');
            $('#modal-btn-guardar-modal-editar').off();
            $('#modal-editar .modal-body').html('<div class="text-center"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>');
            $('#modal-editar').modal('show');
            return true;
        },
        success: function(data) {
            $('#modal-editar .modal-body').html(data);
            $('#modal-btn-guardar-modal-editar').on('click',function() {
                editar_guardar(id);
            })
            return true;
        },
        error: function() {
            $('#modal-editar .modal-body').html('Error obteniendo datos');
            return true;
        }
    });
    return true;
}

function editar_guardar(id) {
        $.ajax({
        type: "GET",
        url: 'cost_types/edit_save',
        data: $('#formulario').serialize(),
        beforeSend: function() {
            //$('#modal-btn-guardar-modal-editar').off();
            $('#modal-btn-guardar-modal-editar').prop("disabled",true);
            $("#modal-editar input").removeClass("is-invalid");
            $('#modal-editar-loading').removeAttr("style");
            return true;
        },
        success: function(data) {
            $('#modal-editar').modal('hide');
            $("#tabla_listado_cost_types").DataTable().ajax.reload();
            toastr.success('Tipo de gasto actualizado correctamente');
            return true;
        },
        error: function(data) {
            if(data.status == 422) {
                $.each(data.responseJSON.errors, function (key, item) {
                    $("#"+key).addClass('is-invalid');
                    toastr.error(item);
                });
            } else {
                $('#modal-editar .modal-body').html('Error desconocido guardando datos: ['+ data.status +']');
            }
            return true;
        },
        complete: function() {
            $('#modal-editar-loading').attr('style','display: none !important');
            $('#modal-btn-guardar-modal-editar').prop("disabled",false);
            return true;
        }
    });
    return true;
}

function eliminar (id, subject) {
    $('#modal-btn-guardar-modal-eliminar').show();
    $('#modal-eliminar .modal-body').html('¿Seguro que quieres eliminar la variedad "'+ subject +'" ? ');
    $('#modal-btn-guardar-modal-eliminar').html('ELIMINAR');
    $('#modal-btn-guardar-modal-eliminar').on('click',function() {
        _eliminar(id);
    })
    $('#modal-eliminar').modal('show');
    return false;
}

function _eliminar (id) {
    $.ajax({
        type: "GET",
        url: 'cost_types/delete',
        data: {id: id},
        beforeSend: function () {
            $('#modal-btn-guardar-modal-eliminar').off();
            $('#modal-btn-guardar-modal-eliminar').prop("disabled",true);
            $('#modal-eliminar .modal-body').html('<div class="text-center"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>');
            return true;
        },

        success: function (data) {
            if (data.errors == 'fail') {
                $('#modal-btn-guardar-modal-eliminar').hide();
                $('#modal-eliminar .modal-body').html(data.message);
            }
            else {
                $('#modal-eliminar').modal('hide');
                $("#tabla_listado_cost_types").DataTable().ajax.reload();
                toastr.success(data.message);
            }
            return true;
        },

        error: function () {
            $('#modal-eliminar .modal-body').html('Error eliminando datos');
            return true;
        },

        complete: function() {
            $('#modal-btn-guardar-modal-eliminar').prop("disabled",false);
            return true;
        }
    });
}
