$(function () {
    cargar_listado_harvests();
});

function cargar_listado_harvests () {

    $("#tabla_listado_harvests").DataTable({
        "ajax": {
            "url": "harvests/list",
            "type": "GET"
        },
        columns: [
            {data: 'harvest', name: 'harvest'},
            {data: 'ingresos', name: 'ingresos', render: DataTable.render.number( '.', ',', 2, '', ' €' )},
            {data: 'gastos', name: 'gastos', render: DataTable.render.number( '.', ',', 2, '', ' €' )},
            {data: 'resultado', name: 'resultado', render: DataTable.render.number( '.', ',', 2, '', ' €' )},
        ],
        "order": [[ 0, "desc" ]],
    });
}
