<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GrapesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('grapes')->truncate();
        DB::table('grapes')->insert(
            [
                [
                'name' => 'Treixadura',
                ],
                [
                'name' => 'Jerez',
                ],
            ]
        );

        DB::table('wineyards')->truncate();
        DB::table('wineyards')->insert(
            [
                [
                'name' => 'Capital',
                ],
                [
                'name' => 'Seara',
                ],
                [
                'name' => 'Pombas',
                ],
            ]
        );
    }
}
