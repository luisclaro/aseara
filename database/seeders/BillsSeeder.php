<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BillsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bills')->truncate();
        DB::table('bills')->insert(
            [
                [
                'identifier' => 'FCUCOPE21110047',
                'date' => '2021/11/30',
                'receptor' => 'Luis Claro',
                'iva' => 12,
                'IRPF' => 2,
                'harvest' => 2020,
                ],
                [
                'identifier' => 'FCUCOPE21120322',
                'date' => '2021/12/16',
                'receptor' => 'Luis Claro',
                'iva' => 12,
                'IRPF' => 2,
                'harvest' => 2021,
                ],
                [
                'identifier' => 'FCU034J45',
                'date' => '2021/12/30',
                'receptor' => 'Luis Claro',
                'iva' => 12,
                'IRPF' => 2,
                'harvest' => 2020,
                ],
            ]
        );

/*
        DB::table('bill_lines')->insert(
            [
                [
                'bill_id' => 1,
                'kg' => 874,
                'grape_id' => 1,
                'wineyard_id' => 1,
                'quality' => 0,
                'alcohol' => 11.3,
                'price' => 1.0171,
                ],
                [
                'bill_id' => 1,
                'kg' => 1126,
                'grape_id' => 1,
                'wineyard_id' => 1,
                'quality' => 0,
                'alcohol' => 11,
                'price' => 1.3034,
                ],
            ]
        );
*/

        DB::table('costs')->truncate();
        DB::table('costs')->insert(
            [
                [
                'gastos' => 1020,
                'year' => 2020,
                'harvest' => 2020,
                'wineyard_id' => 1,
                'cost_type_id' => 2,
                'comment' => '',
                ],
                [
                'gastos' => 800,
                'year' => 2020,
                'harvest' => 2020,
                'wineyard_id' => 1,
                'cost_type_id' => 1,
                'comment' => '',
                ],
            ]
        );

        DB::table('cost_types')->truncate();
        DB::table('cost_types')->insert(
            [
                [
                'subject' => 'Sulfatos',
                ],
                [
                'subject' => 'Abonos',
                ],
                [
                'subject' => 'Adrián',
                ],
                [
                'subject' => 'Otros',
                ],
            ]
        );

        DB::table('payments')->truncate();
        DB::table('payments')->insert(
            [
                [
                'bill_id' => 1,
                'date' => '2021/12/02',
                'amount' => 4417.84,
                ],
                [
                'bill_id' => 1,
                'date' => '2021/12/21',
                'amount' => 1472.61,
                ],
            ]
        );

    }
}
