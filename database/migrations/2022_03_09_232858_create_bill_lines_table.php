<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_lines', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('bill_id');
            $table->unsignedMediumInteger('kg');
            $table->unsignedTinyInteger('grape_id');
            $table->unsignedTinyInteger('wineyard_id');
            $table->unsignedTinyInteger('quality');
            $table->unsignedDecimal('alcohol', $precision = 3, $scale = 1);
            $table->unsignedDecimal('price', $precision =6, $scale = 4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_lines');
    }
};
