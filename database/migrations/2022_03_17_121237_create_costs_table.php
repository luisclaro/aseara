<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('costs', function (Blueprint $table) {
            $table->id();
            $table->unsignedMediumInteger('gastos');
            $table->unsignedSmallInteger('year');
            $table->unsignedSmallInteger('harvest');
            $table->unsignedTinyInteger('wineyard_id');
            $table->string('comment');
            $table->unsignedSmallInteger('cost_type_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('costs');
    }
};
