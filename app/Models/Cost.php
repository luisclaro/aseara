<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cost extends Model
{
    use HasFactory;

    public function cost_type()
    {
        return $this->belongsTo(CostType::class, 'cost_type_id');
    }

    public function wineyard()
    {
        return $this->belongsTo(Wineyard::class, 'wineyard_id');
    }
}
