<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wineyard extends Model
{
    use HasFactory;

    /**
     * Get the costs for the wineyard.
     */
    public function costs()
    {
        return $this->hasMany(Cost::class);
    }

    /**
     * Get the bill_lines for the wineyard.
     */
    public function bill_lines()
    {
        return $this->hasMany(BillLine::class);
    }

    /**
     * Get the users for the wineyards.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
