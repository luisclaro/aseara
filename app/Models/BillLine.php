<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

class BillLine extends Model
{
    use HasFactory;

     protected $appends = ['importe', 'precio_con_iva'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'importe' => 'decimal:2',
        'precio_con_iva' => 'decimal:4',
    ];

    public function bill()
    {
        return $this->belongsTo(Bill::class);
    }

    public function grape()
    {
        return $this->belongsTo(Grape::class);
    }

    public function wineyard()
    {
        return $this->belongsTo(Wineyard::class);
    }

    public function importe(): Attribute
    {
        return Attribute::make(
            get: function ($value, $attributes) {

                return $attributes['price'] * $attributes['kg'];
            }

        );
    }

    public function getPrecioConIvaAttribute()
    {
        $bill = Bill::find($this->bill_id);
        return $this->price * ($bill->iva + 100) /100;
    }

}
