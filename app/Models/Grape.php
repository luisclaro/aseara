<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Grape extends Model
{
    use HasFactory;

    /**
     * Get the bill_lines for grapes.
     */
    public function bill_lines()
    {
        return $this->hasMany(BillLine::class);
    }

    /**
     * Get the users for the grapes.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
