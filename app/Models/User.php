<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function grapes()
    {
        return $this->hasmany(Grape::class);
    }

    public function wineyards()
    {
        return $this->hasmany(Wineyard::class);
    }

    public function bills()
    {
        return $this->hasmany(Bill::class);
    }

    public function bill_lines()
    {
        return $this->hasManyThrough(BillLine::class, Bill::class);
    }

    public function cost_types()
    {
        return $this->hasmany(CostType::class);
    }

    public function costs()
    {
        return $this->hasManyThrough(Cost::class, CostType::class);
    }

    public function payments()
    {
        return $this->hasManyThrough(Payment::class, Bill::class);
    }
}
