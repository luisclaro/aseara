<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CostType extends Model
{
    use HasFactory;

    /**
     * Get the cost types.
     */
    public function costs()
    {
        return $this->hasMany(Cost::class);
    }

    /**
     * Get the users.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
