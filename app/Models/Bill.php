<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
Use Carbon\Carbon;

class Bill extends Model
{
    use HasFactory;

    protected $appends = ['total_bruto', 'iva_total', 'irpf_total'];

    public function bill_lines()
    {
        return $this->hasmany(BillLine::class);
    }

    public function getTotalBrutoAttribute()
    {
        $resumen = 0;
        foreach ($this->bill_lines as $bill_line) {
            $resumen += $bill_line->importe;
        }
        return "{$resumen}";
    }

    public function getIvaTotalAttribute()
    {
        return ($this->total_bruto * $this->iva / 100);
    }

    public function getIrpfTotalAttribute()
    {
        return (($this->total_bruto + $this->iva_total) * $this->irpf / 100 );
    }

    /**
     * Interact with the user's first name.
     *
     * @param  string  $value
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function date(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Carbon::parse($value)->format('d-m-Y'),
            set: fn ($value) => Carbon::parse($value)->format('Y-m-d')
        );
    }

    /**
     * Get the users for the bills.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
