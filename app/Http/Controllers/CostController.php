<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use yajra\Datatables\Datatables;
use App\Models\Cost;
use App\Models\Wineyard;
use App\Models\CostType;

class CostController extends Controller
{
    /**
     * Display a listing of the resource.s
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('costs.index');
    }

    /**
     * Carga listado de costs para la tabla
     * @return json
     */
    public function list()
    {
        $costs = auth()->user()->costs;
        return Datatables::of($costs)
            ->editColumn('cost_subject', function ($costs) {
                return $costs->cost_type->subject;
            })
            ->editColumn('wineyard', function ($costs) {
                return $costs->wineyard->name;
            })
            ->addColumn('acciones', function ($costs) {
                return view('costs.actions',compact('costs'));
            })
            ->make(true);
    }

    /**
     * Carga formulario para nuevo gasto / costs
     * @return json
     */
    public function new()
    {
        $cost_types = auth()->user()->cost_types;
        $wineyards = auth()->user()->wineyards;
        return view('costs.form', compact('cost_types', 'wineyards'));
    }

    /**
     * Guarda formulario de costs
     * @param Request $request
     * @return json
     */
    private function guardar(Request $request, Cost $cost) {
        $validacion = $request->validate([
            'harvest'       => 'required|numeric',
            'year'            => 'required',
            'gastos'          => 'required|numeric|min:0|max:10000000',
            'wineyard_id'   => 'nullable|numeric',
            'cost_type_id'   => 'nullable|numeric',
        ]);

        $cost->harvest = $validacion['harvest'];
        $cost->year = $validacion['year'];
        $cost->gastos = $validacion['gastos'];
        $cost->wineyard_id = $validacion['wineyard_id'];
        $cost->cost_type_id = $validacion['cost_type_id'];
        if ($request->input('comment') !== null) $cost->comment = $request->input('comment');
        else $cost->comment = '';
        $cost->save();
        return true;
    }

    /**
     * Guarda formulario de costs
     * @param Request $request
     * @return json
     */
    public function save(Request $request)
    {
        $cost = new Cost;
        $this->guardar($request, $cost);

        return response()->json([
            'message' => 'ok',
            'errors' => null,
        ], 200);
    }

    /**
     * Edita formulario de costs
     * @param Request $request
     * @return json
     */
    public function edit(Request $request)
    {
        $cost = Cost::get()->find($request->id);
        $cost_types = auth()->user()->cost_types;
        $wineyards = auth()->user()->wineyards;
        return view('costs.form',compact('cost', 'cost_types', 'wineyards'));
    }

    /**
     * Guarda formulario de cost
     * @param Request $request
     * @return boolean
     */
    public function edit_save(Request $request)
    {
        $cost = Cost::find($request->id);
        $this->guardar($request, $cost);

        return response()->json([
            'message' => 'ok',
            'errors' => null,
        ], 200);
    }

    /**
     * Elimina una cost por id
     * @param Request $request
     * @return boolean
     */
    public function delete(Request $request)
    {
        $cost = Cost::find($request->id);
        $message = 'Eliminado gasto: '. $cost->gastos .'€. Cosecha: '.$cost->harvest;

        Cost::destroy($request->id);

        return response()->json([
            'message' => $message,
            'errors' => null,
        ], 200);

        return true;
    }

}
