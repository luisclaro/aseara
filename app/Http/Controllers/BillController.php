<?php

namespace App\Http\Controllers;

use yajra\Datatables\Datatables;
use App\Models\Bill;
use Illuminate\Http\Request;

class BillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('bills.index');
    }

    /**
     * Carga listado de bill_lines para la tabla
     * @return json
     */
    public function list()
    {
        $bills = auth()->user()->bills();
        return Datatables::of($bills)
            ->editColumn('total_bruto', function ($bills) {
                return $bills->total_bruto;
            })
            ->editColumn('iva', function ($bills) {
                return $bills->iva_total;
            })
            ->editColumn('irpf', function ($bills) {
                return (- $bills->irpf_total);
            })
            ->editColumn('total_factura', function ($bills) {
                return ($bills->total_bruto + $bills->iva_total - $bills->irpf_total);
            })
            ->addColumn('acciones', function ($bills) {
                return view('bills.actions', compact('bills'));
            })
            ->make(true);
    }

    /**
     * Carga formulario para nuevo gasto / bills
     * @return json
     */
    public function new()
    {
        return view('bills.form');
    }

    /**
     * Guarda formulario de bills
     * @param Request $request
     * @return json
     */
    public function save(Request $request)
    {
        $bill = new Bill;
        $this->guardar($request, $bill);

        return response()->json([
            'message' => 'ok',
            'errors' => null,
        ], 200);
    }

    /**
     * Guarda formulario de bills
     * @param Request $request
     * @return json
     */
    private function guardar(Request $request, Bill $bill) {
        $validacion = $request->validate([
            'identifier'=> 'required',
            'date'      => 'required|date',
            'harvest'   => 'required|numeric|min:2000|max:3000',
            'receptor'  => 'required',
            'iva'       => 'required|numeric|min:0|max:100',
            'irpf'      => 'required|numeric|min:0|max:100',
        ]);

        $bill->identifier = $validacion['identifier'];
        $bill->date = $validacion['date'];
        $bill->harvest = $validacion['harvest'];
        $bill->receptor = $validacion['receptor'];
        $bill->iva = $validacion['iva'];
        $bill->irpf = $validacion['irpf'];
        $bill->user_id = auth()->id();
        $bill->save();

        return true;
    }

    /**
     * Edita formulario de bills
     * @param Request $request
     * @return json
     */
    public function edit(Request $request)
    {
        $bill = Bill::get()->find($request->id);
        return view('bills.form',compact('bill'));
    }

    /**
     * Guarda formulario de bill
     * @param Request $request
     * @return boolean
     */
    public function edit_save(Request $request)
    {
        $bill = Bill::find($request->id);
        $this->guardar($request, $bill);

        return response()->json([
            'message' => 'ok',
            'errors' => null,
        ], 200);
    }

    /**
     * Elimina una bill por id
     * @param Request $request
     * @return boolean
     */
    public function delete(Request $request)
    {
        $bill = Bill::find($request->id);
        $message = 'Eliminada factura: '. $bill->identifier .'€. Cosecha: '. $bill->harvest;

        $bill_line = $bill->bill_lines->first();

        if ($bill_line) {
            return response()->json([
                'message' => 'No es posible eliminar factura "'. $bill->identifier .'". Linea de factura asociada: '. $bill_line->kg .' kg',
                'errors' => 'fail',
            ], 200);
        }

        Bill::destroy($request->id);

        return response()->json([
            'message' => $message,
            'errors' => null,
        ], 200);

        return true;
    }

    /**
     * Edita formulario de bills
     * @param Request $request
     * @return json
     */
    public function info(Request $request)
    {
        $bill_lines = Bill::find($request->id)->bill_lines;
        return view('bills.info',compact('bill_lines'));
    }

}