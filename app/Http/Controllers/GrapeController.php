<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use yajra\Datatables\Datatables;
use App\Models\Grape;
use App\Http\Controllers\Auth;

class GrapeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('grapes.index');
    }

    /**
     * Carga listado de costs para la tabla
     * @return json
     */
    public function list()
    {
        $grapes = auth()->user()->grapes();
        return Datatables::of($grapes)
            ->addColumn('acciones', function ($grapes) {
                return view('grapes.actions',compact('grapes'));
            })
            ->make(true);
    }

    /**
     * Carga formulario para nuevo gasto / grapes
     * @return json
     */
    public function new()
    {
        return view('grapes.form');
    }

    /**
     * Guarda formulario de grapes
     * @param Request $request
     * @return json
     */
    private function guardar(Request $request, Grape $grape)
    {
        $validacion = $request->validate([
            'name'       => 'required',
        ]);

        $grape->name = $validacion['name'];
        $grape->user_id = auth()->id();
        $grape->save();

        return true;
    }

    /**
     * Guarda formulario de grapes
     * @param Request $request
     * @return json
     */
    public function save(Request $request)
    {
        $grape = new Grape;
        $this->guardar($request, $grape);

        return response()->json([
            'message' => 'ok',
            'errors' => null,
        ], 200);
    }

    /**
     * Edita formulario de grapes
     * @param Request $request
     * @return json
     */
    public function edit(Request $request)
    {
        $grape = Grape::get()->find($request->id);
        return view('grapes.form',compact('grape'));
    }

    /**
     * Guarda formulario de grape
     * @param Request $request
     * @return boolean
     */
    public function edit_save(Request $request)
    {
        $grape = Grape::find($request->id);
        $this->guardar($request, $grape);

        return response()->json([
            'message' => 'ok',
            'errors' => null,
        ], 200);
    }

    /**
     * Elimina una grape por id
     * @param Request $request
     * @return boolean
     */
    public function delete(Request $request)
    {
        $grape = Grape::find($request->id);
        $grape_name = $grape->name;
        $bill_line = $grape->bill_lines->first();

        if ($bill_line) {
            return response()->json([
                'message' => 'No es posible eliminar parcela "'. $grape_name .'". Linea de factura asociada: '.$bill_line->bill->identifier.' - Kg: '. $bill_line->kg,
                'errors' => 'fail',
            ], 200);
        }

        Grape::destroy($request->id);

        return response()->json([
            'message' => 'Eliminada variedad de uva "'. $grape_name .'"',
            'errors' => null,
        ], 200);

        return true;
    }

}
