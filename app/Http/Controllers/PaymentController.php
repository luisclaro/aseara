<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use yajra\Datatables\Datatables;
use App\Models\Payment;
use App\Models\Bill;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('payments.index');
    }

    /**
     * Carga listado de payments para la tabla
     * @return json
     */
    public function list()
    {
        $payments = auth()->user()->payments;
        return Datatables::of($payments)
            ->editColumn('identifier', function ($payments) {
                return $payments->bill->identifier;
            })
            ->addColumn('acciones', function ($payments) {
                return view('payments.actions', compact('payments'));
            })
            ->make(true);
    }

    /**
     * Carga formulario para nuevo pago / payments
     * @return json
     */
    public function new()
    {
        $bills = auth()->user()->bills;
        return view('payments.form', compact('bills'));
    }

    /**
     * Guarda formulario de payments
     * @param Request $request
     * @return json
     */
    public function save(Request $request)
    {
        $payment = new Payment;
        $this->guardar($request, $payment);

        return response()->json([
            'message' => 'ok',
            'errors' => null,
        ], 200);
    }

    /**
     * Guarda formulario de payments
     * @param Request $request
     * @return json
     */
    private function guardar(Request $request, Payment $payment) {
        $validacion = $request->validate([
            'amount'    => 'required|numeric|min:0|max:100000',
            'bill_id'   => 'required',
            'date'      => 'required|date',
        ]);

        $payment->amount = $validacion['amount'];
        $payment->bill_id = $validacion['bill_id'];
        $payment->date = $validacion['date'];
        $payment->save();

        return true;
    }

    /**
     * Edita formulario de payments
     * @param Request $request
     * @return json
     */
    public function edit(Request $request)
    {
        $payment = Payment::get()->find($request->id);
        $bills = auth()->user()->bills;
        return view('payments.form',compact('payment', 'bills'));
    }

    /**
     * Guarda formulario de payment
     * @param Request $request
     * @return boolean
     */
    public function edit_save(Request $request)
    {
        $payment = Payment::find($request->id);
        $this->guardar($request, $payment);

        return response()->json([
            'message' => 'ok',
            'errors' => null,
        ], 200);
    }

    /**
     * Elimina una payment por id
     * @param Request $request
     * @return boolean
     */
    public function delete(Request $request)
    {
        $payment = Payment::find($request->id);
        $message = 'Eliminado pago: '. $payment->amount .'€. Fecha: '. $payment->date;

        Payment::destroy($request->id);

        return response()->json([
            'message' => $message,
            'errors' => null,
        ], 200);

        return true;
    }

}
