<?php

namespace App\Http\Controllers;

use yajra\Datatables\Datatables;
use App\Models\BillLine;
use Illuminate\Http\Request;
use App\Models\Bill;
use App\Models\Grape;
use App\Models\Wineyard;

class BillLineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('bill_lines.index');
    }

    /**
     * Carga listado de bill_lines para la tabla
     * @return json
     */
    public function list()
    {
        $bill_lines = auth()->user()->bill_lines;

        return Datatables::of($bill_lines)
            ->editColumn('date', function ($bill_lines) {
                return $bill_lines->bill->date;
            })
            ->editColumn('identifier', function ($bill_lines) {
                return $bill_lines->bill->identifier;
            })
            ->editColumn('grape', function ($bill_lines) {
                return $bill_lines->grape->name;
            })
            ->editColumn('wineyard', function ($bill_lines) {
                return $bill_lines->wineyard->name;
            })
            ->addColumn('acciones', function ($bill_lines) {
                return view('bill_lines.actions', compact('bill_lines'));
            })
            ->make(true);
    }


    /**
     * Carga formulario para nuevo gasto / bills
     * @return json
     */
    public function new()
    {
        $bills = auth()->user()->bills;
        $grapes = auth()->user()->grapes;
        $wineyards = auth()->user()->wineyards;
        return view('bill_lines.form', compact('bills', 'grapes', 'wineyards'));
    }

    /**
     * Guarda formulario de bill_lines
     * @param Request $request
     * @return json
     */
    public function save(Request $request)
    {
        $bill_line = new BillLine;
        $this->guardar($request, $bill_line);

        return response()->json([
            'message' => 'ok',
            'errors' => null,
        ], 200);
    }

    /**
     * Guarda formulario de bill_lines
     * @param Request $request
     * @return json
     */
    private function guardar(Request $request, BillLine $bill_line) {
        $validacion = $request->validate([
            'bill_id'       => 'required|numeric',
            'kg'            => 'required|numeric',
            'grape_id'      => 'required|numeric',
            'wineyard_id'  => 'required|numeric',
            'quality'       => 'required|numeric',
            'alcohol'       => 'required|numeric',
            'price'         => 'required|numeric',
        ]);

        $bill_line->bill_id = $validacion['bill_id'];
        $bill_line->kg = $validacion['kg'];
        $bill_line->grape_id = $validacion['grape_id'];
        $bill_line->wineyard_id = $validacion['wineyard_id'];
        $bill_line->alcohol = $validacion['alcohol'];
        $bill_line->quality = $validacion['quality'];
        $bill_line->price = $validacion['price'];
        $bill_line->save();

        return true;
    }

    /**
     * Edita formulario de bill_lines
     * @param Request $request
     * @return json
     */
    public function edit(Request $request)
    {
        $bill_line = BillLine::get()->find($request->id);
        $bills = auth()->user()->bills;
        $grapes = auth()->user()->grapes;
        $wineyards = auth()->user()->wineyards;
        return view('bill_lines.form',compact('bill_line', 'bills', 'grapes', 'wineyards'));
    }

    /**
     * Guarda formulario de bill_line
     * @param Request $request
     * @return boolean
     */
    public function edit_save(Request $request)
    {
        $bill_line = BillLine::find($request->id);
        $this->guardar($request, $bill_line);

        return response()->json([
            'message' => 'ok',
            'errors' => null,
        ], 200);
    }

    /**
     * Elimina una bill_line por id
     * @param Request $request
     * @return boolean
     */
    public function delete(Request $request)
    {
        $bill_line = BillLine::find($request->id);
        $message = 'Eliminada línea de factura: '. $bill_line->kg .' kilos.'. $bill_line->alcohol .' grados';

        BillLine::destroy($request->id);

        return response()->json([
            'message' => $message,
            'errors' => null,
        ], 200);

        return true;
    }

}

