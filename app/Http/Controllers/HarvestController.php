<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use yajra\Datatables\Datatables;
use App\Models\Bill;
use App\Models\Cost;

class HarvestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('harvests.index');
    }

    /**
     * Carga listado de harvests para la tabla
     * @return json
     */
    public function list()
    {
        $harvests = auth()->user()->bills()->select('harvest')->groupBy('harvest')->get();
        foreach ($harvests as $harvest) {
            $bills = auth()->user()->bills()->where('harvest', $harvest->harvest)->get();
            $costs = auth()->user()->costs()->where('harvest', $harvest->harvest)->get();
            $harvest->ingresos = $bills->sum('total_bruto');
            $harvest->gastos = $costs->sum('gastos');;
            $harvest->resultado = $harvest->ingresos - $harvest->gastos;
        }
        return Datatables::of($harvests)
            ->make(true);
    }
}
