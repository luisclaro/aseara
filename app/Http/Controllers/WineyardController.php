<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use yajra\Datatables\Datatables;
use App\Models\Wineyard;

class WineyardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('wineyards.index');
    }

    /**
     * Carga listado de wineyards para la tabla
     * @return json
     */
    public function list()
    {
        $wineyards = auth()->user()->wineyards();
        return Datatables::of($wineyards)
            ->addColumn('acciones', function ($wineyards) {
                return view('wineyards.actions',compact('wineyards'));
            })
            ->make(true);
    }

    /**
     * Carga formulario para nuevo gasto / wineyards
     * @return json
     */
    public function new()
    {
        return view('wineyards.form');
    }

    private function guardar(Request $request, Wineyard $wineyard)
    {
        $validacion = $request->validate([
            'name'       => 'required',
        ]);

        $wineyard->name = $validacion['name'];
        $wineyard->user_id = auth()->id();
        $wineyard->save();

        return true;
    }

    /**
     * Guarda formulario de wineyards
     * @param Request $request
     * @return json
     */
    public function save(Request $request)
    {
        $wineyard = new Wineyard;
        $this->guardar($request, $wineyard);

        return response()->json([
            'message' => 'ok',
            'errors' => null,
        ], 200);
    }

    /**
     * Edita formulario de wineyards
     * @param Request $request
     * @return json
     */
    public function edit(Request $request)
    {
        $wineyard = Wineyard::get()->find($request->id);
        return view('wineyards.form',compact('wineyard'));
    }

    /**
     * Guarda formulario de wineyard
     * @param Request $request
     * @return boolean
     */
    public function edit_save(Request $request)
    {
        $wineyard = Wineyard::find($request->id);
        $this->guardar($request, $wineyard);

        return response()->json([
            'message' => 'ok',
            'errors' => null,
        ], 200);
    }

    /**
     * Elimina una wineyard por id
     * @param Request $request
     * @return boolean
     */
    public function delete(Request $request)
    {
        $wineyard = Wineyard::find($request->id);
        $wineyard_name = $wineyard->name;
        $cost = $wineyard->costs->first();
        $bill_line = $wineyard->bill_lines->first();

        if ($cost) {
            return response()->json([
                'message' => 'No es posible eliminar parcela "'. $wineyard_name .'". Gasto asociado: '.$cost->gastos.' - Año: '. $cost->year,
                'errors' => 'fail',
            ], 200);
        }

        if ($bill_line) {
            return response()->json([
                'message' => 'No es posible eliminar parcela "'. $wineyard_name .'". Linea de factura asociada: '.$bill_line->bill->identifier.' - Kg: '. $bill_line->kg,
                'errors' => 'fail',
            ], 200);
        }

        Wineyard::destroy($request->id);

        return response()->json([
            'message' => 'Eliminada parcela "'. $wineyard_name .'"',
            'errors' => null,
        ], 200);

        return true;
    }

}
