<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use yajra\Datatables\Datatables;
use App\Models\CostType;

class CostTypeController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cost_types.index');
    }

    /**
     * Carga listado de costs para la tabla
     * @return json
     */
    public function list()
    {
        $cost_types = auth()->user()->cost_types();
        return Datatables::of($cost_types)
            ->addColumn('acciones', function ($cost_types) {
                return view('cost_types.actions',compact('cost_types'));
            })
            ->make(true);
    }

    /**
     * Carga formulario para nuevo gasto / cost_types
     * @return json
     */
    public function new()
    {
        return view('cost_types.form');
    }

    /**
     * Guarda formulario de cost_types
     * @param Request $request
     * @return json
     */
    private function guardar(Request $request, CostType $cost_type)
    {
        $validacion = $request->validate([
            'subject'       => 'required',
        ]);

        $cost_type->subject = $validacion['subject'];
        $cost_type->user_id = auth()->id();
        $cost_type->save();

        return true;
    }

    /**
     * Guarda formulario de cost_types
     * @param Request $request
     * @return json
     */
    public function save(Request $request)
    {
        $cost_type = new CostType;
        $this->guardar($request, $cost_type);

        return response()->json([
            'message' => 'ok',
            'errors' => null,
        ], 200);
    }

    /**
     * Edita formulario de cost_types
     * @param Request $request
     * @return json
     */
    public function edit(Request $request)
    {
        $cost_type = CostType::get()->find($request->id);
        return view('cost_types.form',compact('cost_type'));
    }

    /**
     * Guarda formulario de cost_type
     * @param Request $request
     * @return boolean
     */
    public function edit_save(Request $request)
    {
        $cost_type = CostType::find($request->id);
        $this->guardar($request, $cost_type);

        return response()->json([
            'message' => 'ok',
            'errors' => null,
        ], 200);
    }

    /**
     * Elimina una cost_type por id
     * @param Request $request
     * @return boolean
     */
    public function delete(Request $request)
    {
        $cost_type = CostType::find($request->id);
        $cost_type_subject = $cost_type->subject;
        $cost = $cost_type->costs->first();

        if ($cost) {
            return response()->json([
                'message' => 'No es posible eliminar tipo de gasto "'. $cost_type_subject .'". Gasto asociado: '.$cost->gastos.'  €. Cosecha '. $cost->harvest,
                'errors' => 'fail',
            ], 200);
        }

        CostType::destroy($request->id);

        return response()->json([
            'message' => 'Eliminado tipo de gasto "'. $cost_type_subject .'"',
            'errors' => null,
        ], 200);

        return true;
    }

}
