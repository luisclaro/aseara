<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use yajra\Datatables\Datatables;
use App\Models\Bill;
use App\Models\Cost;
use App\Models\Payment;

class YearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('years.index');
    }

    /**
     * Carga listado de years para la tabla
     * @return json
     */
    public function list()
    {
        $years = auth()->user()->bills()->select('harvest')->groupBy('harvest')->get();
        foreach ($years as $year) {
            $payments = auth()->user()->payments()->whereYear('payments.date', '=', $year->harvest)->get();
            $costs = auth()->user()->costs()->where('year', $year->harvest)->get();
            $year->ingresos = $payments->sum('amount');
            $year->gastos = - $costs->sum('gastos');;
            $year->resultado = $year->ingresos + $year->gastos;
        }
        return Datatables::of($years)
            ->make(true);
    }
}
